import React from 'react';
import './index.css';
import {  TodoList } from "../";

const App = () => {
  return (
    <div className="App">
      <h1>Todo App</h1>
        <TodoList />
    </div>
  );
}

export default App;
