import { Todo } from "../TodoList";

interface TodoItemProps  {
    todo: Todo;
    onRemove: (id: number) => void;
    onCheck: (id: number) => void;
}
const TodoItem = ({todo, onRemove, onCheck}: TodoItemProps) => {
    return <div>
        <input type="checkbox" checked={todo.done} onChange={() => onCheck(todo.id)}/>
        <span>{todo.text}</span>
        <button onClick={() => onRemove(todo.id)}>Remove</button>
    </div>
}

export default TodoItem;