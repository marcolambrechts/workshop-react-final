import {ChangeEvent, useState} from "react";

interface AddTodoProps {
    onAddTodo: (text: string) => void;
}
const AddTodo = ({onAddTodo}: AddTodoProps) => {
    const [value, setValue] = useState<string>("");

    const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
        setValue(e.target.value);
    }

    return <>
        <input type="text" onChange={handleChange} value={value}/>
        <button onClick={() => {onAddTodo(value); setValue("")}}>Add TODO</button>
    </>;
}

export default AddTodo;