import {useEffect, useState} from "react";
import {AddTodo, TodoItem} from "../index";

export type Todo = {
    id: number;
    text: string;
    done: boolean;
}

const TodoList = () => {
    const [todos, setTodos] = useState<Todo[]>([]);
    const [isMounted, setIsMounted] = useState<boolean>(false);


    useEffect(() => {
        const storedTodos = localStorage.getItem('todos');

        if(storedTodos !== null) {
            const savedTodos = JSON.parse(storedTodos);
            if (savedTodos) {
                setTodos(savedTodos);
            }
        }

        setIsMounted(true);
    }, []);

    useEffect(() => {
        if(!isMounted) {
            return;
        }

        localStorage.setItem('todos', JSON.stringify(todos));
    }, [todos]);

    const addTodo = (text: string) => {
        const newTodo = {
            id: Date.now(),
            text,
            done: false
        }
        setTodos([...todos, newTodo]);
    }

    const removeTodo = (id: number) => {
        setTodos(todos.filter(todo => todo.id !== id));
    }

    const checkTodo = (id: number) => {
        setTodos(todos.map(todo => {
            if (todo.id === id) {
                return {
                    ...todo,
                    done: !todo.done
                }
            }
            return todo;
        }))
    }

    return <div>
        <AddTodo onAddTodo={addTodo} />
        {
            todos.map((todo) => (
                <div key={todo.id}>
                    <TodoItem todo={todo} onCheck={checkTodo} onRemove={removeTodo} />
                </div>
            ))
        }
    </div>
}

export default TodoList;